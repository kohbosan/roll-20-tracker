import json
import logging

import SimpleWebSocketServer

fmt = logging.Formatter('%(levelname)-7s - %(asctime)s - %(message)s')
log = logging.getLogger()
log.setLevel(logging.DEBUG)
flog = logging.FileHandler("error_log", "w")
flog.setLevel(logging.DEBUG)
clog = logging.StreamHandler()
clog.setLevel(logging.INFO)
flog.setFormatter(fmt)
clog.setFormatter(fmt)
log.addHandler(flog)
log.addHandler(clog)

clients = []


class Monitor(SimpleWebSocketServer.WebSocket):
    def handleMessage(self):
        log.info("Message from " + str(self.address) + " -> " + self.data)
        try:
            data = json.loads(self.data)
            if data['type'] == "update":
                for client in clients:
                    client.sendMessage(self.data)
            if data['type'] == "message":
                log.info(data['msg'])
            if data['type'] == "error":
                log.error(data['msg'])
        except json.decoder.JSONDecodeError:
            log.error("Invalid JSON format")
            log.error(self.data)
        except KeyError:
            log.exception("Invalid JSON keys.")
            log.error(self.data)


    def handleConnected(self):
        log.info(str(self.address) + " Connected...")
        clients.append(self)
        self.sendMessage(json.dumps(
            {'type': 'message',
             'msg': 'Connected to server.'}))
        self.sendMessage(json.dumps(
            {'type': 'message',
             'msg': 'Remember to open your character sheet to begin sending your stats.'}))

    def handleClose(self):
        try:
            clients.remove(self)
        except ValueError:
            log.warning("Tried to remove client not in list.")
            log.warning(self)
            log.warning(clients)

    def broadcastMsg(self, msg):
        for client in clients:
            client.sendMessage(msg)


log.info("Launching server...")
srv = SimpleWebSocketServer.SimpleWebSocketServer('', 8000, Monitor)
srv.serveforever()

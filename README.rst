Tool for syncing character information across computers using Roll 20.

[Chrome Web Store Link](https://chrome.google.com/webstore/detail/roll-20-tracker/eecleikaecgljkagkgdmmchljoakjgnk?hl=en-US&gl=US&authuser=0)

![Screenshot](https://bitbucket.org/repo/BAd8Ma/images/3088324222-2017-03-09%2013_08_01-Roll%2020%20Tracker%20-%20Chrome%20Web%20Store.png)
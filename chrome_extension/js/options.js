angular.module("TrackerOptions", [])
.controller("TrackerOptions", ["$scope", function($scope){
	$scope.options = {};
	var options = {
            stats: {
                hp: true,
                inspiration: true,
                hp_temp: true,
                ac: true,
                pb: true
            }
        };
	chrome.storage.local.get(options, function(items){
		console.debug(items);
		$scope.options = items;

		// Necessary to apply to ensure inputs match $scope.
		$scope.$apply();
	});
	$scope.$watch("options", function(n){
		chrome.storage.local.set($scope.options, function(){
			console.debug($scope.options);
		});
	}, true);
}]);
// Holds the logic for parsing the json messages and displaying them on the game field.
"use strict";

(function () {
    // Inject tracker div into DOM
    var trk_div = document.createElement("div");
    trk_div.id = "tracker";
    trk_div.setAttribute("ng-app", "tracker");
    trk_div.setAttribute("ng-controller", "Tracker");
    trk_div.innerHTML =
        '<div ng-repeat="(name, stat) in characters" ng-class="{trk_inspired: stat.inspiration, statbox: true}">' +
        '<strong>{{name}}</strong>' +
        '<div ng-If="options.stats.hp"><label>HP</label><div><span class="overlay">{{stat.hp}}/{{stat.hp_max}}</span><meter min="0" low="{{stat.hp_max|perc:3}}" max="{{stat.hp_max}}" high="{{stat.hp_max|perc:6}}" value="{{stat.hp}}"></meter></div></div>' +
        '<div ng-If="options.stats.hp_temp && stat.hp_temp > 0"><label>HPT</label><div><span class="overlay">{{stat.hp_temp}}</span><meter min="0" max="{{stat.hp_max}}" value="{{stat.hp_temp}}"></meter></div></div>' +
        '<div ng-If="options.stats.ac"><label>AC</label><div>{{stat.ac}}</div></div>' +
        '<div ng-If="options.stats.pb"><label>PB</label><div>{{stat.pb}}</div></div>' +
        '</div>';
    document.body.appendChild(trk_div);
    console.debug(trk_div);

    // Inject Toast handler
    (function () {
        var e = document.getElementById("tracker").cloneNode(false);
        e.id = "trk_toast_holder";
        document.body.appendChild(e);
        document.body.addEventListener("trkToast", function (ev) {
            var t = document.createElement("div");
            t.className = "toast";
            t.addEventListener("animationend", function (e) {
                e.target.parentNode.removeChild(e.target);
            });
            var x = document.createElement("div");
            x.addEventListener("click", function (e) {
                e.target.parentNode.parentNode.removeChild(e.target.parentNode);
            });
            t.appendChild(x);
            t.appendChild(document.createTextNode(ev.detail.msg));
            e.appendChild(t);
        });
        return e;
    })();

    function Tracker($scope) {
        // Define dictionary to hold character stats
        $scope.characters = {};

        // Load the options from chrome storage.
        // chrome.storage.local.get(null, function(items){
        //     console.debug(JSON.stringify(items));
        //     $scope.options = items;
        // });
        $scope.options = {
            "stats": {
                "hp": true,
                "hp_temp": true,
                "ac": true,
                "pb": true
            }
        };

        //Define Message Port and listeners
        var port = (function () {
            chrome.runtime.onConnect.addListener(function () {
                console.log("Message port opened");
            });
            var p = chrome.runtime.connect({name: "tracker"});
            p.onDisconnect.addListener(function () {
                console.log("Message port closed");
            });
            return p;
        })();

        port.onMessage.addListener(function (msg_json) {
            console.debug("New Message: " + msg_json);
            var msg = JSON.parse(msg_json);

            if (msg.type == "update") {
                // Add character to characters if missing
                if (!$scope.characters.hasOwnProperty(msg.char_name)) {
                    $scope.characters[msg.char_name] = {};
                }
                // Iterate over stats from webSocket and set in dictionary
                for (var stat in msg.stats) {
                    $scope.characters[msg.char_name][stat] = msg.stats[stat];
                }
                console.log(JSON.stringify($scope.characters));
                $scope.$apply();
            }
            else if (msg.type == "message") {
                $scope.createToastEvent(msg.msg);
            }
        });

        $scope.createToastEvent = function (msg) {
            document.body.dispatchEvent(new CustomEvent(
                "trkToast", {
                    detail: {
                        msg: msg
                    },
                    bubbles: true,
                    cancelable: false
                }
            ));
        };
    }


    // Initialize Angular
    var app = angular.module("tracker", []);
    app.controller("Tracker", ["$scope", "$filter", Tracker]);
    app.filter("perc", function () {
        return function (val, perc) {
            return parseInt(parseInt(val) / parseInt(perc));
        };
    });

    // Manually bootstrap Angular due to injected Tracker DOM element
    // angular.bootstrap(trk_div, ["tracker"]);
})();

// function Tracker() {
//     setInterval(function () {
//         for (var e of this.trackerdiv.children) {
//             if (Date.now() - e.dataset['trk_update'] > 30000) {
//                 e.parentNode.removeChild(e);
//                 trk.createToastEvent("No data for " + e.dataset['c_name'] + " received for 30 seconds. Removing from tracker.");
//             }
//         }
//     }.bind(this), 5000);
//     this.createToastEvent = function (msg) {
//         document.body.dispatchEvent(new CustomEvent(
//             "trkToast", {
//                 detail: {
//                     msg: msg
//                 },
//                 bubbles: true,
//                 cancelable: false
//             }
//         ));
//     };
// }

chrome.runtime.onConnect.addListener(function (port) {
    console.assert(port.name == "tracker");
    console.log("Port opened");
    var ws = openWS(port);
    port.onMessage.addListener(function (msg) {
        console.log("Port message Received");
        console.debug(msg);
        ws.send(msg);
        console.log("Port message forwarded to WebSocket.");
    });
    port.onDisconnect.addListener(function () {
        console.log("Port closed. Closing WebSocket.");
        ws.close();
    });
});

function openWS(port) {
    // var ws = new WebSocket("ws://localhost:8000");
    var ws = new WebSocket("ws://kohbo.ddns.net:8000");
    ws.onopen = function () {
        console.log("Websocket open.");
        ws.send(JSON.stringify({type: "message", "msg": "Running version 0.2.5"}));
    };
    ws.onmessage = function (event) {
        console.log("Websocket Message Received");
        console.debug(event.data);
        port.postMessage(event.data);
        console.log("WebSocket message forwarded to port.");
    };
    ws.onclose = function () {
        console.log("Websocket closed.");
        port.disconnect();
        setTimeout(function () {
            console.log("Attempting to reconnect.");
        }, 3000);
    };
    ws.onerror = function (event) {
        console.error("Websocket Error");
        console.error(event);
    };
    return ws;
}
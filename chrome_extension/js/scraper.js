// Holds the logic for pulling character information off the sheet and sending it to the server.

function Scraper() {
    this.getStatElement = function (attr) {
        return document.querySelector("input[name=attr_" + attr + "]");
    };
    this.char_name = this.getStatElement("character_name").value;
    this.stats = {
        inspiration: 'checked',
        hp: 'value',
        hp_max: 'value',
        hp_temp: 'value',
        ac: 'value',
        pb: 'value'
    };
    this.updateTracker = function () {
        console.log("Sending update for " + this.char_name);
        var update = {
            type: "update",
            char_name: this.char_name,
            stats: {}
        };
        for (var prop in this.stats) {
            if (this.stats.hasOwnProperty(prop)) {
                var val = this.getStatElement(prop)[this.stats[prop]];
                console.debug(prop + ": " + val);
                if (!isNaN(parseInt(val)) || this.stats[prop] == "checked") {
                    update.stats[prop] = val;
                } else {
                    this.port.postMessage(JSON.stringify({
                        "type": "error",
                        "msg": "Error getting " + prop + " for " + this.char_name
                    }));
                    update.stats[prop] = 0;
                }
            }
        }
        this.port.postMessage(JSON.stringify(update));
    };
    this.port = (function () {
        chrome.runtime.onConnect.addListener(function () {
            console.log("Message port opened");
        });
        var p = chrome.runtime.connect({name: "tracker"});
        p.onDisconnect.addListener(function () {
            console.log("Message port closed");
        });
        return p;
    })();
    this.beginUpdates = function () {
        if (!confirm("Tracking will begin for " + this.char_name + ". Click OK to continue.")) {
            return;
        }
        console.log("Starting updates for " + this.char_name);
        for (var prop in this.stats) {
            if (this.stats.hasOwnProperty(prop)) {
                this.getStatElement(prop).addEventListener("change", function (e) {
                    console.debug("Event triggered update");
                    this.updateTracker();
                }.bind(this));
                // update.stats[prop] = self.getStatElement(prop)[self.stats[prop]];
            }
        }
        setInterval(this.updateTracker.bind(this), 10000);
    }
}

new Scraper().beginUpdates();